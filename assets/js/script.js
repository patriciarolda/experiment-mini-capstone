function yourName() {
    let visitorName = prompt("Hey! What's your name? (Don't worry! This is not recorded!)");
    
    if (visitorName === "") {
        document.getElementById("visitorName").innerHTML = "Hello Lazy-Person-Who-Doesn't-Want-to-Type-Her/His-Name!";
    }else if (visitorName){
        document.getElementById("visitorName").innerHTML = "HELLO " + visitorName.toUpperCase();
    }else{
        document.getElementById("visitorName").innerHTML = "Your name should be here but you clicked cancel. I'll call you Mr./Ms. Cancel!";
    }
}

yourName();

// CHANGE VISITOR NAME
function changeName(){
    let visitorName = prompt("Alright! You can change your name. (Again, this is not recorded) Don't be shy.");
    if (visitorName === "") {
        document.getElementById("visitorName").innerHTML = "You've accidentally clicked OK.";
    }else if (visitorName){
        document.getElementById("visitorName").innerHTML = "Finally! HELLO again, " + visitorName.toUpperCase() + "!";
    }else{
        document.getElementById("visitorName").innerHTML = "Seriously, you've clicked CANCEL.";
    }

}

function about() {
    let about = document.getElementById("about");
    if (about.style.display === "none") {
      about.style.display = "block";
    } else {
      about.style.display = "none";
    }
  }

function popupJoke() {
    var popup = document.getElementById("popuptext");
    popup.classList.toggle("show");
  }

// CLICK 10 TIMES
var count = 1;
function tenTimes() {
    if (count < 10) {
        count++;
    }else{
        window.open("./mini-capstone/to-do-list/index.html", "_blank");
        count = 1;
    }
};


// FIND THE RIGHT BUTTON 


function gradiantMode() {
    let color = document.body;

    color.classList.remove("blue-mode");
    color.classList.remove("yellow-mode");
    color.classList.remove("pink-mode");
    color.classList.remove("cucumber-mode");
    color.classList.remove("fresco-mode");
    color.classList.remove("sea-mode");
    color.classList.remove("lilly-mode");
    color.classList.remove("cactus-mode");
    color.classList.add("gradiant-mode");
    
}

function blueMode() {
    let color = document.body;

    
    color.classList.remove("yellow-mode");
    color.classList.remove("pink-mode");
    color.classList.remove("cucumber-mode");
    color.classList.remove("fresco-mode");
    color.classList.remove("sea-mode");
    color.classList.remove("lilly-mode");
    color.classList.remove("cactus-mode");
    color.classList.remove("gradiant-mode");
    color.classList.add("blue-mode");
    
}

function yellowMode() {
    let color = document.body;
    color.classList.remove("blue-mode");
    color.classList.remove("pink-mode");
    color.classList.remove("cucumber-mode");
    color.classList.remove("fresco-mode");
    color.classList.remove("sea-mode");
    color.classList.remove("lilly-mode");
    color.classList.remove("cactus-mode");
    color.classList.remove("gradiant-mode");
    color.classList.add("yellow-mode");
}

function pinkMode() {
    let color = document.body;
    color.classList.remove("blue-mode");
    color.classList.remove("yellow-mode");
    color.classList.remove("cucumber-mode");
    color.classList.remove("fresco-mode");
    color.classList.remove("sea-mode");
    color.classList.remove("lilly-mode");
    color.classList.remove("cactus-mode");
    color.classList.remove("gradiant-mode");
    color.classList.add("pink-mode");
}

function cucumberMode() {
    let color = document.body;
    color.classList.remove("blue-mode");
    color.classList.remove("yellow-mode");
    color.classList.remove("pink-mode");
    color.classList.remove("fresco-mode");
    color.classList.remove("sea-mode");
    color.classList.remove("lilly-mode");
    color.classList.remove("cactus-mode");
    color.classList.remove("gradiant-mode");
    color.classList.add("cucumber-mode");
}

function frescoMode() {
    let color = document.body;
    color.classList.remove("blue-mode");
    color.classList.remove("yellow-mode");
    color.classList.remove("pink-mode");
    color.classList.remove("cucumber-mode");
    color.classList.remove("sea-mode");
    color.classList.remove("lilly-mode");
    color.classList.remove("cactus-mode");
    color.classList.remove("gradiant-mode");
    color.classList.add("fresco-mode");
}

function seaMode() {
    let color = document.body;
    color.classList.remove("blue-mode");
    color.classList.remove("yellow-mode");
    color.classList.remove("pink-mode");
    color.classList.remove("cucumber-mode");
    color.classList.remove("fresco-mode");
    color.classList.remove("lilly-mode");
    color.classList.remove("cactus-mode");
    color.classList.remove("gradiant-mode");
    color.classList.add("sea-mode");
}

function lillyMode() {
    let color = document.body;
    color.classList.remove("blue-mode");
    color.classList.remove("yellow-mode");
    color.classList.remove("pink-mode");
    color.classList.remove("cucumber-mode");
    color.classList.remove("fresco-mode");
    color.classList.remove("sea-mode");
    color.classList.remove("cactus-mode");
    color.classList.remove("gradiant-mode");
    color.classList.add("lilly-mode");
}

function cactusMode() {
    let color = document.body;
    color.classList.remove("blue-mode");
    color.classList.remove("yellow-mode");
    color.classList.remove("pink-mode");
    color.classList.remove("cucumber-mode");
    color.classList.remove("fresco-mode");
    color.classList.remove("sea-mode");
    color.classList.remove("lilly-mode");
    color.classList.remove("gradiant-mode");
    color.classList.add("cactus-mode");
}

function reset() {
    let color = document.body;
    color.classList.remove("blue-mode");
    color.classList.remove("yellow-mode");
    color.classList.remove("pink-mode");
    color.classList.remove("cucumber-mode");
    color.classList.remove("fresco-mode");
    color.classList.remove("sea-mode");
    color.classList.remove("lilly-mode");
    color.classList.remove("gradiant-mode");
    color.classList.remove("cactus-mode");
}

function correctPage() {
    alert("YEY! YOU'VE GOT IT RIGHT!");
}

function showButton() {
    var button = document.getElementById("show-button");
        if (button.style.display === "none") {
        button.style.display = "block";
        } else {
        button.style.display = "none";
        }    
  }